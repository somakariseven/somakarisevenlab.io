## #StayAtHome

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
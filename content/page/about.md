---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Seven. 


### Now... I’m focusing on:

- [Python 3](https://www.codecademy.com/learn/learn-python-3)
- [Kungfu](https://www.udemy.com/course/kung-fu-shaolin-level-1-part-1/)
- Meditating & eating healthy vegetarian food


### I’m currently reading:

- [The Asperkid’s (Secret) Book of Social Rules](https://www.goodreads.com/book/show/14903347-the-asperkid-s-secret-book-of-social-rules) by Jennifer Cook O’Toole


### I’m currently watching:

- [Marvel - The Defenders](https://www.netflix.com/es/title/80002566) on Netflix


Last updated: Apr 05, 2020
---
title: Problem Solver
date: 2020-04-12
tags: ["project"]
bigimg: [{src: "/img/problem-min.jpg", desc: "Rubik"}]
---

¿Le das vueltas a algo que no te deja dormir? ¿Lo has consultado con el I Ching, pero no te ha convencido su respuesta?
Deja de preocuparte, ha llegado... 

<!--more-->

["Problem Solver"](https://somakari.gitlab.io/home/), la página web que te ayudará con eso que no te deja dormir...



*Header photo by [Michelen Studios](https://unsplash.com/@michelenstudios) on Unsplash.*

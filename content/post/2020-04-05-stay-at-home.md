---
title: Stay At Home
subtitle: let's do some cool stuff
date: 2020-04-05
tags: ["project"]
bigimg: [{src: "/img/header-min.jpg", desc: "Computer"}]
---

Tools, OS and IT stuff, privacy and security that you didn’t have time to start on your day-to-day routine and that maybe now you can try some in quarantine.
This idea is based on a series of twitter posts created by [@Terceranexus6](https://twitter.com/Terceranexus6/status/1239146322432864257).

<!--more-->


* [ ]  Linux OS with Virtual Box.

* [ ]  Introduction to the [command-line](https://tutorial.djangogirls.org/en/intro_to_command_line/) interface.

* [ ]  Mozilla web browser with: duckduckgo, htppseverywhere, privacy badger, hoxx vpn proxy, and mailvelope.

* [ ]  Create a [Mastodon](https://mastodon.social/about) account, the open-source social network service.

* [ ]  Design a website on [GitLab](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/).

* [ ]  Learn programming with [Python](https://www.codecademy.com/learn/learn-python-3).

* [ ]  Creating a [telegram bot](https://medium.com/python4you/creating-telegram-bot-and-deploying-it-on-heroku-471de1d96554) using Python, Heroku and Docker.

* [ ]  [Magic-Wormhole](https://github.com/warner/magic-wormhole): Get things from one computer to another, safely.

* [ ]  Learning about Information Security: [Follow The White Rabbit](https://fwhibbit.es/en/) blog.

* [ ]  [Web Security Academy](https://portswigger.net/web-security)by PortSwigger.



*Header image by [Lorenzo Herrera](https://unsplash.com/@lorenzoherrera) on Unsplash.*